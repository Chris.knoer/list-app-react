# list-app
This is an incredibly simple React application, the frontend companion to [list-app-express](https://github.com/gSchool/list-app-express)

## Install
After cloning into the root directory of the project, install dependencies.
```
npm install
```
Create a .env file with the following value for local server connection or rename the example.env file
```
REACT_APP_API_URL=http://localhost:5000
```

## Usage
- **npm start** - to launch a dev server, serving the application
- **npm run build** - to create a production optimized build, output to the build directory